import React from "react";

function Tags(props) {
  return (
    <div class="tags">
      {props.tags.map((tag) => (
        <p class="tag">#{tag}</p>
      ))}
    </div>
  );
}

export default Tags;
